$(function () {
    $('[data-toggle="tooltip"]').tooltip();
    $('[data-toggle="popover"]').popover();
    $('.carousel').carousel({
        interval: 3000,
        pause: false
    });
    $('#exampleModal').on('show.bs.modal', function (e) {
        $('#btnContacto').removeClass('btn-success')
        $('#btnContacto').addClass('btn-warning')
        console.log("El modal se está abriendo")
    })
    $('#exampleModal').on('shown.bs.modal', function (e) {
        $('#btnContacto').prop('disabled', true)
        console.log("El modal se abrió")
    })
    $('#exampleModal').on('hide.bs.modal', function (e) {
        $('#btnContacto').removeClass('btn-warning')
        $('#btnContacto').addClass('btn-success')
        console.log("El modal se está ocultando")
    })
    $('#exampleModal').on('hidden.bs.modal', function (e) {
        $('#btnContacto').prop('disabled', false)
        console.log("El modal se ocultó")
    })
})